// Contains all the endpoints for our application

// we need to use express Router() function to achieve this

const express = require('express');


// Creates a Router instance that function as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application.


const router = express.Router();


const taskController = require('../controllers/taskController');


/*
	Syntax : localhost:2000/tasks
*/

// [SECTION] Routes


router.get('/getinfo', (req, res) => {
	taskController.getallTasks().then(resultFromController => res.send(resultFromController));
})



// Route to create a new task
router.post('/create', (req, res) => {

	// if information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
	taskController.createTask(req.body).then(result => res.send(result));
})

// Route to delete a task

// the colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
// The word that comes after the colon (:) symbol will be the name of the URL parameter
// ":id" is a wildcard where you can put any value, its then creates a link between "id" parameter in the URL and the value provided in the URL

router.delete("/delete/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})


// ====================================================================================
//           A C T I V I T Y
// ====================================================================================

router.put("/update/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

router.get("/getinfo/:id", (req, res) => {
	taskController.infoTask(req.params.id).then(result => res.send(result));
})

// Use "module.exports" to export the router object to use in the "app.js"

module.exports = router;