// Controllers contain the functions and Business logic of our express JS. Meaning all the operation it can do will be placed in this file.

const Task = require('../models/task')

module.exports.getallTasks = () => {
	// The return statement, returns the result of the Mongoose Method
	// Then "then" method is used to wait for the Mongoose Method to finish before sending the result back to the route.
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	console.log(requestBody);
	// Create a task object based on the Mongoose Model "Tasks"
	let newTask = new Task({

		name : requestBody.name
	})

	return newTask.save().then((task, error) => {

		if (error){
			console.log(error);

		// if an error is encountered, the "return" statement will prevent any other line or code within the same code block
		// The else statement will no longer be evaluated
			return false;
		} else {
			return task
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then ((result, err) => {
		if (err) {
			console.log(err);
			return false;
		} else {
			return result
		}
	})
}

// ====================================================================================
//           A C T I V I T Y
// ====================================================================================

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {

		if (err) {
			console.log(err);
			return false;
		} else {

			if (newContent.status)	{
				result.status = newContent.status;	
			}
			
			if(newContent.name) {
				result.name = newContent.name;
			}
			
			return result.save().then((updated, saveErr) => {
				if (saveErr) {
					console.log(saveErr);
					return false;
				} else {
					console.log(updated);
					return updated;
				}
			})

		}
	})
}

module.exports.infoTask = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if (err) {
			console.log(err);
			return false;
		} else {
			return result
		}
	})
}