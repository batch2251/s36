// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();

// created routes
const taskRoute = require("./routes/taskRoute")

const app = express();
const port = 2000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection
// Connect to MongoDB atlas
mongoose.set('strictQuery', true);

mongoose.connect(`mongodb+srv://${process.env.M_USERNAME}:${process.env.M_PASSWORD}@cluster0.z8dag7j.mongodb.net/MONGOOSE?retryWrites=true&w=majority`,
    {
        useNewUrlParser : true,
        useUnifiedTopology : true
  }
);

// Setup notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on('open', () => console.log("Connected to MongoDB!"));

// Add the task routes
// Allows all the task routes created in the "taskroute.js" file to use "/tasks" route

app.use("/tasks", taskRoute);

app.listen(port, () => console.log (`Now listening to port ${port}...`));